## Question 5

I used this script to decode the message

```python
hex_string = '4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c20616772'\
             '6567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a616465'\
             '6c736f6c61722e636f6d22207061726120736162657220717565206c6c6567617374652061206573'\
             '74612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f72'\
             '20617520c3a9212e'

decoded_string = bytes.fromhex(hex_string).decode('latin-1')
print(decoded_string)
```

the output was:

```
Este es el último paso, por favor, agregame al hangout:

"martin@mendozadelsolar.com" para saber que llegaste a esta parte.

Gracias, y espero verte por au Ã©!.
```
