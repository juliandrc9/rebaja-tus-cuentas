## Question 1

The last time I used RTFM was earlier today when I needed to refresh my memory on the syntax for a specific Python library. Rather than asking a colleague for help, I first consulted the documentation to see if I could find the answer on my own.

Similarly, I recently used LMGTFY when a friend who is not a programmer asked me a basic question about programming languages. Instead of answering the question directly, I sent him a link to a search engine query that provided a list of relevant resources on the topic.

As a Linux user, I am comfortable with command-line tools and often rely on them for tasks like file management, process monitoring, and system administration.

Finally, as a developer who masters Python, C++, and JavaScript, I am proficient in a wide range of programming paradigms and have experience working on projects ranging from web applications to backend systems.