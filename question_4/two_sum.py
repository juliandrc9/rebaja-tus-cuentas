def find_pair(numbers, target_sum):
    left, right = 0, len(numbers) - 1
    while left < right:
        current_sum = numbers[left] + numbers[right]
        if current_sum == target_sum:
            return numbers[left], numbers[right]
        elif current_sum < target_sum:
            left += 1
        else:
            right -= 1
    return None

# Example usage
numbers = [2,3,6,7]
target_sum = 9
pair = find_pair(numbers, target_sum)
if pair:
    print(f"Ok, matching pair {pair}")
else:
    print("No")