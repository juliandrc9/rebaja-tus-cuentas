
## Question 4

### Code

```python
def find_pair(numbers, target_sum):
    left, right = 0, len(numbers) - 1
    while left < right:
        current_sum = numbers[left] + numbers[right]
        if current_sum == target_sum:
            return numbers[left], numbers[right]
        elif current_sum < target_sum:
            left += 1
        else:
            right -= 1
    return None
```
This solution is memory optimized because it iterates over the same array.

### Complexity

Lex `n` be the number of elements in the input array.

- The time complexity of this solution is `O(n)`.
- The space complexity of this solution is `O(n)`.
