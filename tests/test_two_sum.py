from typing import List, Optional

import pytest

from question_4.two_sum import find_pair


@pytest.mark.parametrize(
    "numbers, target_sum, expected_answer",
    [
        ([3, 6, 9, 13], 16, (3, 13)),
        ([2, 4], 5, None),
        ([5, 17, 22, 25], 30, (5, 25)),
    ],
)
def test_two_sum(numbers: List[int], target_sum: int, expected_answer: Optional[int]):
    """
    GIVEN   an array of numbers and a `target_sum`
    WHEN    the `find_pair` function is called
    THEN    a pair of numers with sum equal to `target_sum` should be returned if any,
            otherwise None should be returned
    """
    # GIVEN
    # WHEN
    returned_answer = find_pair(numbers=numbers, target_sum=target_sum)

    # THEN
    assert returned_answer == expected_answer
