## Question 2

```yaml
image: python:3.9

stages:
  - test

before_script:
  - pip install -r requirements.txt

test:
  stage: test
  script:
    - pylint *.py # Run linter
    - pytest # Run tests
```

In this script:

- `image: python:3.9` specifies the Docker image to use for the build environment. In this case, we are using the official Python 3.9 image.

- `before_script` defines commands that are run before each job in the pipeline. Here, we are installing the Python requirements using pip.

- `test` is the name of the job. We have defined only one job here, but you can add additional jobs with different stages as needed.

- `stage: test` defines the stage of the job, which is "test" in this case.

- `script` defines the commands that are run as part of the job. Here, we are first running pylint to check the code for errors, and then running pytest to run the tests.

When this script is added to a GitLab repository with CI enabled, it will run the linter and tests every time a commit is pushed to the repository.