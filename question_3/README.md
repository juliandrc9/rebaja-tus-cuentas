## Question 3

This is just a small part of the whole project but it contains the main parts of it

[Project folder](https://gitlab.com/juliandrc9/rebaja-tus-cuentas/-/tree/main/question_3/r10)

### Tech stack

- Python 3
- Fast API
- Django
- PostgreSQL
- Redis


The project consists of 3 layers:
- `api`: Implementation of endpoints, such as, definition of the route, request method, returned response model, returned response code, error responses, etc.
- `business_logic`: Consists of two layers
    - `adapters`: Convert Django Queryset to a pydantic model
    - `controllers`: Here goes the business logic, a function here can use one or multiple adapters
- `dao`:
    - `controllers`: Communication with the database, all its functions return a Django Queryset
    - `models`: Django Model definition
- `tests`: Each function is tested here



The proyect uses:

- FastAPI to implement RESTFull APIs and also it implement automatic documentation
- Django ORM to manage database tables and queries
- PostgreSQL to store data
- Redis to cache queries

The project implement for CI workflow:
- Lint
- Tests
