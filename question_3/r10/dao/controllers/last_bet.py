from collections.abc import Sequence
from typing import Optional, cast
from uuid import UUID

from django.db.models import Q, QuerySet

from dao.env_settings import redis_settings
from dao.models import LastBetModel
from dao.models.cache import CustomCachingQuerySet


def fetch_last_bets(
    match_id: Optional[UUID] = None,
    cache_timeout: Optional[int] = None,
    extra_filter: Optional[Q] = None,
) -> QuerySet[LastBetModel]:
    """
    Return the last bets from the given `match`

    A general `extra_filter` for the queryset can be added.
    """
    last_bets = LastBetModel.objects.all()

    if match_id is not None:
        last_bets = last_bets.filter(
            match__id=match_id, company__is_enabled=True
        ).exclude(company__image_url="")

    if extra_filter is not None:
        last_bets = last_bets.filter(extra_filter)

    if redis_settings.REDIS_HOST and cache_timeout is not None:
        last_bets = cast(CustomCachingQuerySet, last_bets)
        last_bets.timeout = cache_timeout

    return last_bets


def bulk_create_last_bets(last_bets: Sequence[LastBetModel]) -> None:
    """
    Bulk create `last_bets`
    """
    LastBetModel.objects.bulk_create(last_bets)


def bulk_update_last_bets(
    last_bets: Sequence[LastBetModel], fields: Sequence[str]
) -> None:
    """
    Bulk update `fields` of `last_bets`
    """
    LastBetModel.objects.bulk_update(last_bets, list(fields) + ["updated_at"])
