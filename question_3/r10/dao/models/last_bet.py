from __future__ import annotations

import uuid

from caching.base import CachingMixin
from django.db import models
from django.db.models.manager import BaseManager

from dao.env_settings import timeout_settings
from dao.models.auditable_model import AuditableModel
from dao.models.bet import Bet
from dao.models.cache import CustomCachingManager, CustomCachingQuerySet
from dao.models.match import Match
from dao.models.odd_company import OddCompany
from dao.models.typing.choices import annotate_text_choice


class LastBetCachingQuerySet(CustomCachingQuerySet):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.timeout = timeout_settings.REAL_TIMEOUT


class LastBetCachingManager(CustomCachingManager):
    def get_queryset(self):
        return LastBetCachingQuerySet(self.model, using=self._db)


class LastBet(CachingMixin, AuditableModel):
    objects: BaseManager[LastBet] = LastBetCachingManager()

    class Meta:
        base_manager_name = "objects"
        db_table = "r10_last_bet"
        constraints = [
            models.UniqueConstraint(
                fields=["match", "company", "kind", "option"],
                name="Unique last bet",
            )
        ]

    # Id
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True,
    )

    # Attributes
    match = models.ForeignKey(
        Match,
        on_delete=models.CASCADE,
    )
    match_id: uuid.UUID  # type hint

    company = models.ForeignKey(
        OddCompany,
        on_delete=models.CASCADE,
    )
    kind = annotate_text_choice(max_length=200, choice=Bet.Kind)
    timing = annotate_text_choice(max_length=200, choice=Bet.Timing)
    odd = models.DecimalField(max_digits=5, decimal_places=2)
    detail = models.CharField(max_length=200, null=True)
    option = annotate_text_choice(max_length=200, choice=Bet.Option)
