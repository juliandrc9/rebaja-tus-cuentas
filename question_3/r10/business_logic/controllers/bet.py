import logging
from typing import Optional
from uuid import UUID

from dao.models import BetOption, Language
from rest_backend.business_logic.adapters import (
    bet_adapter,
    match_adapter,
    odd_company_adapter,
)
from rest_backend.exceptions import ObjectNotFound
from rest_backend.schemas import (
    POSSIBLE_KINDS,
    CompanyBet,
    OptionCompanyBet,
    SimplifiedBet,
)
from rest_backend.schemas.bet import GroupedBets
from utils.collections import align_with_many, align_with_one, assert_equal_length

logger = logging.getLogger(__name__)

BET365_NAME = "BET365"


def fetch_bets(language: Optional[Language], match_id: UUID) -> GroupedBets:
    """
    Fetch the bet information related to the given `match`, grouped by `option`
    and then by `company`.
    - The list of `CompanyBet` will be sorted by the `name`.
    """
    match_adapter.check_match(match_id=match_id)

    last_bets = bet_adapter.fetch_last_bets(language=language, match_id=match_id)

    companies = list({last_bet.company for last_bet in last_bets})
    bet_options = list(BetOption)

    last_bets_aligned = align_with_many(
        left=bet_options, right=last_bets, right_key=lambda b: b.option
    )
    assert_equal_length(bet_options, last_bets_aligned)

    grouped_bets: list[OptionCompanyBet] = []
    for option, company_bet in zip(bet_options, last_bets_aligned):
        company_bets_aligned = align_with_many(
            left=companies, right=company_bet, right_key=lambda b: b.company
        )
        assert_equal_length(companies, company_bets_aligned)

        company_bets: list[CompanyBet] = []
        for company, bets in zip(companies, company_bets_aligned):
            simplified_bets = [SimplifiedBet(**bet.__dict__) for bet in bets]

            if len(simplified_bets) == 0:
                continue

            if option in [BetOption.GOAL_LINE, BetOption.ASIAN_CORNER]:
                details = list(
                    set([simplified_bet.detail for simplified_bet in simplified_bets])
                )
                if len(details) != 1:
                    logger.warning(
                        f"{company.name} skipped in {option} from "
                        f"match with id {match_id} due to bets related are empty or "
                        "there are more than two details."
                    )
                    logger.warning(simplified_bets)
                    continue

            company_bets.append(
                CompanyBet(
                    company=company,
                    bets=align_with_one(
                        left=POSSIBLE_KINDS[option],
                        right=simplified_bets,
                        right_key=lambda b: b.kind,
                    ),
                )
            )

        company_bets.sort(key=lambda b: b.company.name)
        grouped_bets.append(OptionCompanyBet(option=option, company_bets=company_bets))

    # Add BETANO company
    try:
        betano_company = odd_company_adapter.fetch_odd_company(
            name="BETANO",
            language=language,
        )
    except ObjectNotFound:
        betano_company = None

    for grouped_bet in grouped_bets:
        for company_bet in grouped_bet.company_bets:
            if company_bet.company.name == BET365_NAME and betano_company:
                grouped_bet.company_bets.append(
                    CompanyBet(company=betano_company, bets=company_bet.bets)
                )

    return GroupedBets(grouped_bets=grouped_bets, match_id=match_id)
