from typing import Optional
from uuid import UUID

from dao.controllers import last_bet_controller
from dao.env_settings import timeout_settings
from dao.models import Language
from rest_backend.schemas import LastBet


def fetch_last_bets(language: Optional[Language], match_id: UUID) -> list[LastBet]:
    """
    Adapter to return the last bets related to the given match
    """

    last_bets_db = last_bet_controller.fetch_last_bets(
        match_id=match_id, cache_timeout=timeout_settings.NEAR_REAL_TIMEOUT
    )
    return LastBet.from_orms(last_bets_db, language=language)


def exists_bets(match_id: UUID) -> bool:
    """
    Adapter to check if there are bets related to the given `match`.
    """
    last_bets = last_bet_controller.fetch_last_bets(
        match_id=match_id, cache_timeout=timeout_settings.NEAR_REAL_TIMEOUT
    )
    return last_bets.count() != 0
