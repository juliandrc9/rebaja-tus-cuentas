import pytest

from dao.factories import LastBetFactory, MatchFactory, OddCompanyFactory
from rest_backend.business_logic.adapters.bet import exists_bets


@pytest.mark.parametrize("is_empty", [True, False])
@pytest.mark.django_db(transaction=True)
def test_exists_bets(is_empty: bool):
    """
    GIVEN   a match with zero or some bets
    WHEN    the `exists_bets` function is called
    THEN    it should return `True` iff the match has at least one bet
    """
    # GIVEN
    match = MatchFactory.create()

    odd_company = OddCompanyFactory.create(is_enabled=not is_empty)
    LastBetFactory.create(match=match, company=odd_company)

    # WHEN
    # THEN
    assert not is_empty == exists_bets(match_id=match.id)
