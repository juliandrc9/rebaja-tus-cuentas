import pytest
from factory import SubFactory

from dao.factories import LastBetFactory, MatchFactory, OddCompanyFactory
from rest_backend.business_logic.adapters.bet import fetch_last_bets
from utils.collections import equal_lists_by_keys


@pytest.mark.django_db(transaction=True)
def test_fetch_bets():
    """
    GIVEN   a match, companies and a list of bets
    WHEN    the `fetch_bets` function is called
    THEN    only the bets of the given match should be returned
    """
    # GIVEN
    target_match = MatchFactory.create()
    n_target_bets = 8
    expected_last_bets = LastBetFactory.create_batch(
        size=n_target_bets,
        company=SubFactory(OddCompanyFactory, is_enabled=True, image_url="test"),
    )

    n_other_bets = 7
    LastBetFactory.create_batch(
        size=n_other_bets,
        match=SubFactory(MatchFactory),
        company=SubFactory(OddCompanyFactory, is_enabled=True, image_url="test"),
    )

    # WHEN
    returned_last_bets = fetch_last_bets(language=None, match_id=target_match.id)

    # THEN
    assert equal_lists_by_keys(
        returned_last_bets,
        expected_last_bets,
        key=lambda bet: (bet.match_id, bet.kind, bet.company.name, bet.option),
    )


@pytest.mark.django_db(transaction=True)
def test_bets_non_image_url():
    """
    GIVEN   a match, companies and a list of bets and bets with odd companies
            with non image_url
    WHEN    the `fetch_bets` function is called
    THEN    only the bets with odd company with image_url should be returned
    """
    # GIVEN
    target_match = MatchFactory.create()
    n_target_bets = 8
    expected_last_bets = LastBetFactory.create_batch(
        size=n_target_bets,
        company=SubFactory(OddCompanyFactory, is_enabled=True, image_url="test"),
    )

    n_other_bets = 7
    LastBetFactory.create_batch(
        size=n_other_bets,
        match=target_match,
        company=SubFactory(OddCompanyFactory, is_enabled=False, image_url="test"),
    )

    # WHEN
    returned_last_bets = fetch_last_bets(language=None, match_id=target_match.id)

    # THEN
    assert equal_lists_by_keys(
        returned_last_bets,
        expected_last_bets,
        key=lambda bet: (bet.match_id, bet.kind, bet.company.name, bet.option),
    )


@pytest.mark.django_db(transaction=True)
def test_bets_not_enabled():
    """
    GIVEN   a match, companies and a list of bets and bets with enabled and not enabled
            odd companies
    WHEN    the `fetch_bets` function is called
    THEN    only the bets with enabled odd companies should be returned
    """
    # GIVEN
    target_match = MatchFactory.create()
    n_target_bets = 8
    expected_last_bets = LastBetFactory.create_batch(
        size=n_target_bets,
        company=SubFactory(OddCompanyFactory, is_enabled=True, image_url="test"),
    )

    n_other_bets = 7
    LastBetFactory.create_batch(
        size=n_other_bets,
        match=target_match,
        company=SubFactory(OddCompanyFactory, is_enabled=True, image_url=""),
    )

    # WHEN
    returned_last_bets = fetch_last_bets(language=None, match_id=target_match.id)

    # THEN
    assert equal_lists_by_keys(
        returned_last_bets,
        expected_last_bets,
        key=lambda bet: (bet.match_id, bet.kind, bet.company.name, bet.option),
    )
