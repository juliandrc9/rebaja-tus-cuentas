import logging

import pytest
from pytest import LogCaptureFixture

from dao.factories import LastBetFactory, MatchFactory, OddCompanyFactory
from dao.models import BetOption, LastBetModel, OddCompanyModel
from rest_backend.business_logic.controllers.bet import fetch_bets
from rest_backend.schemas import POSSIBLE_KINDS, SimplifiedBet
from utils.errors import InternalInvalidDataError
from utils.exceptions import InternalInvalidData


@pytest.mark.django_db(transaction=True)
def test_fetch_bets():
    """
    GIVEN   a match, companies and a list of bets
    WHEN    the `fetch_bets` function is called
    THEN    the bets should be returned
    """

    # GIVEN
    OddCompanyFactory.create(name="BETANO", image_url="image-url", is_enabled=True)

    match = MatchFactory.create()
    companies = OddCompanyFactory.create_batch(
        3, image_url="image-url", is_enabled=True
    )
    for company in companies:
        for option in list(BetOption):
            for kind in POSSIBLE_KINDS[option]:
                LastBetFactory.create(
                    company=company,
                    option=option,
                    kind=kind,
                    detail="3.5",
                )

    # WHEN
    grouped_bets = fetch_bets(language=None, match_id=match.id).grouped_bets

    # THEN
    total_returned_bets = 0
    for grouped_bet in grouped_bets:
        for company_bet in grouped_bet.company_bets:
            total_returned_bets += len(company_bet.bets)

    assert LastBetModel.objects.all().count() == total_returned_bets


@pytest.mark.parametrize(
    "company_names, given_option_companies, expected_grouping",
    [
        (
            ["name1", "name2", "name3", "name4", "name5", "ignored"],
            [
                (BetOption.ASIAN_CORNER, "name1"),
                (BetOption.FULL_TIME, "name2"),
                (BetOption.GOAL_LINE, "name3"),
                (BetOption.FULL_TIME, "name4"),
                (BetOption.ASIAN_CORNER, "name5"),
            ],
            [
                (BetOption.FULL_TIME, ["name2", "name4"]),
                (BetOption.ASIAN_HANDICAP, []),
                (BetOption.GOAL_LINE, ["name3"]),
                (BetOption.ASIAN_CORNER, ["name1", "name5"]),
            ],
        ),
        (
            ["ignored"],
            [],
            [
                (BetOption.FULL_TIME, []),
                (BetOption.ASIAN_HANDICAP, []),
                (BetOption.GOAL_LINE, []),
                (BetOption.ASIAN_CORNER, []),
            ],
        ),
    ],
)
@pytest.mark.django_db(transaction=True)
def test_grouped_by_option(
    company_names: list[str],
    given_option_companies: list[tuple[BetOption, str]],
    expected_grouping: list[tuple[BetOption, list[str]]],
):
    """
    GIVEN   a match, companies and a list of `given_option_companies` to be created
    WHEN    the `fetch_bets` function is called
    THEN    the bets should be returned, grouping by option and filter by companies
            with bets should be checked
    """

    # GIVEN
    OddCompanyFactory.create(name="BETANO", image_url="image-url", is_enabled=True)
    assert len(expected_grouping) == len(list(BetOption)), "Wrong parameter"

    match = MatchFactory.create()
    for name in company_names:
        OddCompanyFactory.create(name=name, image_url="image-url", is_enabled=True)

    for option, company_name in given_option_companies:
        company = OddCompanyModel.objects.get(name=company_name)
        for kind in POSSIBLE_KINDS[option]:
            LastBetFactory.create(
                company=company, option=option, kind=kind, detail="3.5"
            )

    # WHEN
    returned_grouped_bets = fetch_bets(language=None, match_id=match.id).grouped_bets

    # THEN
    returned_grouping = [
        (
            grouped_bet.option,
            [company_bets.company.name for company_bets in grouped_bet.company_bets],
        )
        for grouped_bet in returned_grouped_bets
    ]

    assert returned_grouping == expected_grouping


@pytest.mark.parametrize(
    "company_names, given_option",
    [
        (
            ["Macauslot", "Easybets", "Wewbet", "188bet"],
            BetOption.ASIAN_HANDICAP,
        ),
    ],
)
@pytest.mark.django_db(transaction=True)
def test_grouped_by_company(
    company_names: list[str],
    given_option: BetOption,
):
    """
    GIVEN   a match, companies and a list of `given_option_companies` to be created
    WHEN    the `fetch_bets` function is called
    THEN    the bets should be returned, also grouping by option and sorting by
            company name should be checked
    """

    # GIVEN
    OddCompanyFactory.create(name="BETANO", image_url="image-url", is_enabled=True)
    match = MatchFactory.create()
    for name in company_names:
        OddCompanyFactory.create(name=name, image_url="image-url", is_enabled=True)

    expected_grouping: list[tuple[str, list[SimplifiedBet]]] = []
    for company_name in company_names:
        company = OddCompanyModel.objects.get(name=company_name)

        expected_simplified_bets: list[SimplifiedBet] = []
        for kind in POSSIBLE_KINDS[given_option]:
            last_bet = LastBetFactory.create(
                company=company, option=given_option, kind=kind, detail="3.5"
            )
            expected_simplified_bets.append(SimplifiedBet(**last_bet.__dict__))

        expected_grouping.append((company_name, expected_simplified_bets))
    expected_grouping.sort(key=lambda b: b[0])

    # WHEN
    returned_grouped_bets = fetch_bets(language=None, match_id=match.id).grouped_bets

    # THEN
    returned_grouping: list[tuple[str, list[SimplifiedBet]]] = []
    for grouped_bet in returned_grouped_bets:
        if grouped_bet.option == given_option:
            for company_bet in grouped_bet.company_bets:
                returned_grouping.append(
                    (
                        company_bet.company.name,
                        company_bet.bets,
                    )
                )
        else:
            assert len(grouped_bet.company_bets) == 0

    assert returned_grouping == expected_grouping


@pytest.mark.django_db(transaction=True)
def test_invalida_data():
    """
    GIVEN   existing match, companies and a list of bets that does not have all
            the necessary kinds
    WHEN    the `fetch_bets` function is called
    THEN    an `InvalidData` error should be raised
    """

    # GIVEN
    OddCompanyFactory.create(name="BETANO", image_url="image-url", is_enabled=True)
    match = MatchFactory.create()
    companies = OddCompanyFactory.create_batch(
        size=2, image_url="image-url", is_enabled=True
    )

    for company in companies:
        for option in list(BetOption):
            LastBetFactory.create(
                company=company,
                option=option,
                kind=POSSIBLE_KINDS[option][0],
                detail="3.5",
            )

    # WHEN
    # THEN
    with pytest.raises(
        expected_exception=InternalInvalidData,
        match=InternalInvalidDataError.NO_MAPPING_IN_ALIGNMENT.value.message,
    ):
        fetch_bets(language=None, match_id=match.id)


@pytest.mark.parametrize(
    "company_names",
    [
        (["BET365", "Macauslot"]),
    ],
)
@pytest.mark.django_db(transaction=True)
def test_warning_logs(caplog: LogCaptureFixture, company_names: list[str]):
    """
    GIVEN   existing match, companies and a list of bets with invalid detail data
    WHEN    the `fetch_bets` function is called
    THEN    a warning should de raised and the bets should be skipped
    """

    # GIVEN
    OddCompanyFactory.create(name="BETANO", image_url="image-url", is_enabled=True)
    match = MatchFactory.create()
    for name in company_names:
        OddCompanyFactory.create(name=name, image_url="image-url", is_enabled=True)

    for company_name in company_names:
        company = OddCompanyModel.objects.get(name=company_name)
        for pos, kind in enumerate(POSSIBLE_KINDS[BetOption.ASIAN_CORNER]):
            LastBetFactory.create(
                company=company,
                option=BetOption.ASIAN_CORNER,
                kind=kind,
                detail=str(pos),
            )
        for pos, kind in enumerate(POSSIBLE_KINDS[BetOption.GOAL_LINE]):
            LastBetFactory.create(
                company=company,
                option=BetOption.GOAL_LINE,
                kind=kind,
                detail=str(pos),
            )

    # WHEN
    caplog.clear()
    with caplog.at_level(logging.WARNING):
        bets_returned = fetch_bets(language=None, match_id=match.id).grouped_bets

    # THEN
    assert (
        sum(
            len(option_company_bet.company_bets) for option_company_bet in bets_returned
        )
        == 0
    )
    assert len(caplog.records) == len(company_names) * 2 * 2
    assert "WARNING" in caplog.text
    assert "bets related are empty or there are more than two details" in caplog.text


@pytest.mark.parametrize(
    "company_option_list, expected_company_option_list",
    [
        (
            [
                ("BET365", BetOption.ASIAN_CORNER),
                ("BET365", BetOption.ASIAN_HANDICAP),
                ("ChuntalaYa", BetOption.FULL_TIME),
                ("ChuntalaYa", BetOption.GOAL_LINE),
            ],
            [
                ("BET365", BetOption.ASIAN_CORNER),
                ("BETANO", BetOption.ASIAN_CORNER),
                ("BET365", BetOption.ASIAN_HANDICAP),
                ("BETANO", BetOption.ASIAN_HANDICAP),
                ("ChuntalaYa", BetOption.FULL_TIME),
                ("ChuntalaYa", BetOption.GOAL_LINE),
            ],
        ),
    ],
)
@pytest.mark.django_db(transaction=True)
def test_add_betano_company(
    company_option_list: list[tuple],
    expected_company_option_list: list[tuple],
):
    """
    GIVEN   a match, companies and a list of `given_option_companies` to be created
    WHEN    the `fetch_bets` function is called
    THEN    the bets should be returned, the addition of `BETANO` company should be
            checked
    """

    # GIVEN
    OddCompanyFactory.create(name="BETANO", image_url="image-url", is_enabled=True)
    match = MatchFactory.create()
    OddCompanyFactory.create(name="BET365", image_url="image-url", is_enabled=True)
    OddCompanyFactory.create(name="ChuntalaYa", image_url="image-url", is_enabled=True)

    for company_name, option in company_option_list:
        company = OddCompanyModel.objects.get(name=company_name)
        for kind in POSSIBLE_KINDS[option]:
            LastBetFactory.create(
                company=company, option=option, kind=kind, detail="3.5"
            )

    # WHEN
    returned_grouped_bets = fetch_bets(language=None, match_id=match.id).grouped_bets

    # THEN
    returned_company_option_list: list[tuple] = []
    for grouped_bet in returned_grouped_bets:
        for company_bet in grouped_bet.company_bets:
            returned_company_option_list.append(
                (company_bet.company.name, grouped_bet.option)
            )

    returned_company_option_list.sort(key=lambda x: (x[1], x[0]))

    assert expected_company_option_list == returned_company_option_list
