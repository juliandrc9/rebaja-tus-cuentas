from uuid import uuid4

import pytest
from fastapi import status
from fastapi.testclient import TestClient

from dao.factories import BetFactory, MatchFactory, OddCompanyFactory
from rest_backend.env_settings import settings
from rest_backend.errors import ObjectNotFoundError
from rest_backend.exceptions import ObjectNotFound
from rest_backend.schemas import GroupedBets

base_url = f"{settings.MAIN_API_URL_PREFIX}/bets"


@pytest.mark.django_db(transaction=True)
def test_fetch_bets(client: TestClient):
    """
    GIVEN   existing match and bets
    WHEN    the GET /bets/{match_id}/ endpoint is called with all its parameters
    THEN    the response should be `HTTP_200_OK`
    """
    # GIVEN
    OddCompanyFactory.create(name="BETANO", image_url="image-url", is_enabled=True)
    num_bets = 10
    match = MatchFactory.create()
    BetFactory.create_batch(size=num_bets)

    # WHEN
    response = client.get(f"{base_url}/{match.id}/")

    # THEN
    content = response.json()

    assert response.status_code == status.HTTP_200_OK, content
    GroupedBets(**content)


@pytest.mark.django_db(transaction=True)
def test_fetch_bets_non_existent_match(client: TestClient):
    """
    GIVEN   no matches
    WHEN    the GET /bets/{match_id}/ endpoint is called with all its parameters
    THEN    the response should be `HTTP_404_NOT_FOUND`
    """

    # GIVEN
    num_bets = 10
    non_existent_match_id = uuid4()
    BetFactory.create_batch(size=num_bets)

    # WHEN
    response = client.get(f"{base_url}/{non_existent_match_id}/")

    # THEN
    content = response.json()

    assert response.status_code == status.HTTP_404_NOT_FOUND, content
    assert (
        content["detail"]
        == ObjectNotFound(error=ObjectNotFoundError.MATCH_NOT_FOUND).detail
    )
