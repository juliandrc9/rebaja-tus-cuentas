from uuid import UUID

from fastapi import APIRouter, Depends, Path, status

from rest_backend.api.exceptions import HTTPNotFoundException, generate_documentation
from rest_backend.business_logic.controllers import bet_controller
from rest_backend.business_logic.controllers.auth import get_current_user
from rest_backend.errors import ObjectNotFoundError
from rest_backend.exceptions import ObjectNotFound
from rest_backend.schemas import GroupedBets, UserWithRole

router = APIRouter()


@router.get(
    "/{match_id}/",
    status_code=status.HTTP_200_OK,
    response_model=GroupedBets,
    responses=generate_documentation(
        documented_exceptions=[
            HTTPNotFoundException.document_exceptions(
                exceptions=[
                    ObjectNotFound(
                        error=ObjectNotFoundError.MATCH_NOT_FOUND,
                    )
                ]
            )
        ]
    ),
)
def fetch_bets(
    user: UserWithRole = Depends(get_current_user()),
    match_id: UUID = Path(..., title="Match ID", description="Id of the target match"),
):
    """
    Return the bet information related to the given `match`, grouped by `option`
    and then by `company`.
    - The list of `CompanyBet` will be sorted by the `name`.
    """

    try:
        return bet_controller.fetch_bets(language=user.language, match_id=match_id)
    except ObjectNotFound as e:
        raise HTTPNotFoundException(detail=e.detail) from e
