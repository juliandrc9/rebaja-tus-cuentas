from fastapi import APIRouter

from rest_backend.api.routes.r10_score import (
    ads,
    back_office,
    banner,
    bets,
    competition_seasons,
    competitions,
    guest,
    incidents,
    language,
    link_tracker,
    login,
    match,
    notifications,
    players,
    pro,
    stats,
    subscriptions,
    teams,
    users,
)
from rest_backend.env_settings import settings

router = APIRouter()


@router.get("/", tags=["Health Check"])
def health_check():
    return {"message": f"It Works! (v{settings.MAIN_API_VERSION})"}


router.include_router(
    login.router,
    prefix="/login",
    tags=["Login"],
)

router.include_router(
    users.router,
    prefix="/users",
    tags=["Users"],
)

router.include_router(
    players.router,
    prefix="/players",
    tags=["Players"],
)

router.include_router(
    match.router,
    prefix="/matches",
    tags=["Matches"],
)

router.include_router(
    competitions.router,
    prefix="/competitions",
    tags=["Competitions"],
)

router.include_router(
    competition_seasons.router,
    prefix="/competition-seasons",
    tags=["Competition Seasons"],
)


router.include_router(
    teams.router,
    prefix="/teams",
    tags=["Teams"],
)

router.include_router(
    stats.router,
    prefix="/stats",
    tags=["Stats"],
)

router.include_router(
    incidents.router,
    prefix="/incidents",
    tags=["Incidents"],
)

router.include_router(
    bets.router,
    prefix="/bets",
    tags=["Bets"],
)


router.include_router(
    pro.router,
    prefix="/pro",
    tags=["Pro"],
)

router.include_router(
    subscriptions.router,
    prefix="/subscriptions",
    tags=["Subscriptions"],
)

router.include_router(
    guest.router,
    prefix="/guest",
    tags=["Guest"],
)

router.include_router(
    notifications.router,
    prefix="/notifications",
    tags=["Notifications"],
)

router.include_router(
    back_office.router,
    prefix="/back-office",
    tags=["Back office"],
)

router.include_router(
    banner.router,
    prefix="/banners",
    tags=["Banners"],
)

router.include_router(
    link_tracker.router,
    prefix="/link-tracker",
    tags=["Link tracker"],
)

router.include_router(
    language.router,
    prefix="/language",
    tags=["Language"],
)

router.include_router(
    ads.router,
    prefix="/ads",
    tags=["Ads"],
)
